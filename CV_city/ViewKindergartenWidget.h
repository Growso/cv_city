#pragma once

#include "VeiwInstitutionWidget.h"
#include "Kindergarten.h"

class ViewKindergartenWidget :
	public ViewInstitutionWidget
{
public:
	ViewKindergartenWidget(Kindergarten* kind);

	~ViewKindergartenWidget();

	void show() override;

private:
	Kindergarten * m_kind_ptr;
};
