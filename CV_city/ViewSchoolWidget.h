#pragma once

#include "VeiwInstitutionWidget.h"
#include "School.h"

class ViewSchoolWidget :
	public ViewInstitutionWidget
{
public:
	ViewSchoolWidget(School* school);

	~ViewSchoolWidget();

	void show() override;

private:
	School * m_school_ptr;
};


