#pragma once

#include "ViewBuildingWidget.h"
#include "Apartment.h"

class ViewApartmentWidget :
	public ViewBuildingWidget
{
public:
	ViewApartmentWidget(Apartment* app);

	~ViewApartmentWidget();

	void show() override;

private:
	Apartment * m_app_ptr;
};

