#pragma once

#include "Object.h"

class Object;

class AbstractWidget {
public:
	AbstractWidget(Object *obj) {}
	
	~AbstractWidget() {}

	virtual void show() = 0;
};