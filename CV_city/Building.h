#pragma once

#include "AbstractCityItem.h"

class Building :
	public AbstractCityItem
{
public:
	virtual ~Building() {}

	virtual void setFloors(int floors) = 0;
	virtual int getFloors() const = 0;

protected:
	std::string		m_adress;
	int				m_floors;
};

