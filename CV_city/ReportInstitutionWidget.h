#pragma once
#include "Institution.h"

class ReportInstitutionWidget :
	public AbstractWidget{
public:
	ReportInstitutionWidget(Institution *inst);

	virtual ~ReportInstitutionWidget();

	virtual void report();

	virtual void show();

private:
	Institution*		 m_inst_ptr;
};