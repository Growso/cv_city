#pragma once

#include "ReportInstitutionWidget.h"
#include "University.h"

class ReportUniversityWidget :
	public ReportInstitutionWidget
{
public:

	ReportUniversityWidget(University *school);

	~ReportUniversityWidget();

	void report() override;

	void show() override;

private:
	University * m_school_ptr;
};

