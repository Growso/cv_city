#include "Object.h"
#include "Stop.h"
#include "SpecialConstruction.h"
#include "Apartment.h"
#include "Office.h"
#include "Kindergarten.h"
#include "School.h"
#include "University.h"

#include "ViewObjectWidget.h"
#include "ViewStopWidget.h"
#include "ViewSpecialConstructionWidget.h"
#include "ViewBuildingWidget.h"
#include "ViewApartmentWidget.h"
#include "ViewOfficeWidget.h"
#include "VeiwInstitutionWidget.h"
#include "ViewKindergartenWidget.h"
#include "ViewSchoolWidget.h"
#include "ViewUniversityWidget.h"

#include "ReportInstitutionWidget.h"
#include "ReportKindergartenWidget.h"
#include "ReportSchoolWidget.h"
#include "ReportUniversityWidget.h"

#include "ListWidget.h"

#include <list>
#include <stdexcept>
#include <iostream>
#include <string>


Object::Object()
{
}

Object::~Object()
{
}

void Object::setWidget(ViewObjectWidget * ptr)
{
	m_widget_ptr = ptr;
}

ViewObjectWidget * Object::getWidget() const
{
	return m_widget_ptr;
}

std::string Object::typeString() const
{
	return std::string("Object");
}

char Object::img() const
{
	return '0';
}

Stop::Stop()
{
}


Stop::~Stop()
{
}

void Stop::setAdress(std::string adress)
{
	m_adress = adress;
}

std::string Stop::getAdress() const
{
	return m_adress;
}

void Stop::setRoutes(std::list<int> routes)
{
	m_routes = routes;
}

std::list<int> Stop::getRoutes() const
{
	return m_routes;
}

void Stop::setWidget(ViewStopWidget * ptr)
{
	m_widget_ptr = ptr;
}

ViewStopWidget * Stop::getWidget() const
{
	return m_widget_ptr;
}

std::string Stop::typeString() const
{
	return std::string("Stop");
}

char Stop::img() const
{
	return 's';
}

SpecialConstruction::SpecialConstruction()
{
}


SpecialConstruction::~SpecialConstruction()
{
}

void SpecialConstruction::setAdress(std::string adress)
{
	m_adress = adress;
}

std::string SpecialConstruction::getAdress() const
{
	return m_adress;
}

void SpecialConstruction::setFactory(std::string factory)
{
	m_factory = factory;
}

std::string SpecialConstruction::getFactory() const
{
	return m_factory;
}

void SpecialConstruction::setWidget(ViewSpecialConstructionWidget * ptr)
{
	m_sp_costr_ptr = ptr;
}

ViewSpecialConstructionWidget * SpecialConstruction::getWidget() const
{
	return m_sp_costr_ptr;
}

std::string SpecialConstruction::typeString() const
{
	return std::string("Special Construction");
}

char SpecialConstruction::img() const
{
	return 'c';
}

Apartment::Apartment()
{
}


Apartment::~Apartment()
{
}

void Apartment::setFlats(int flats)
{
	m_flats = flats;
}

int Apartment::getFlats() const
{
	return m_flats;
}

void Apartment::setAdress(std::string adress)
{
	m_adress = adress;
}

std::string Apartment::getAdress() const
{
	return m_adress;
}

void Apartment::setFloors(int floors)
{
	m_floors = floors;
}

int Apartment::getFloors() const
{
	return m_floors;
}

void Apartment::setWidget(ViewApartmentWidget * ptr)
{
	m_appartment_ptr = ptr;
}

ViewApartmentWidget * Apartment::getWidget() const
{
	return m_appartment_ptr;
}

std::string Apartment::typeString() const
{
	return std::string("Apartment");
}

char Apartment::img() const
{
	return 'a';
}

Office::Office()
{
}


Office::~Office()
{
}

void Office::setCompanies(std::list<std::string> companies)
{
	m_companies = companies;
}

std::list<std::string> Office::getCompanies() const
{
	return m_companies;
}

void Office::setAdress(std::string adress)
{
	m_adress = adress;
}

std::string Office::getAdress() const
{
	return m_adress;
}

void Office::setFloors(int floors)
{
	m_floors = floors;
}

char Office::img() const
{
	return 'f';
}

int Office::getFloors() const
{
	return m_floors;
}

void Office::setWidget(ViewOfficeWidget * ptr)
{
	m_office_ptr = ptr;
}

ViewOfficeWidget * Office::getWidget() const
{
	return m_office_ptr;
}

std::string Office::typeString() const
{
	return std::string("Office");
}

Kindergarten::Kindergarten()
{
}


Kindergarten::~Kindergarten()
{
}

void Kindergarten::setAdress(std::string adress)
{
	m_adress = adress;
}

std::string Kindergarten::getAdress() const
{
	return m_adress;
}

void Kindergarten::setFloors(int floors)
{
	m_floors = floors;
}

int Kindergarten::getFloors() const
{
	return m_floors;
}

void Kindergarten::setName(std::string name)
{
	m_name = name;
}

std::string Kindergarten::getName() const
{
	return m_name;
}

void Kindergarten::setNursery(bool nursery)
{
	m_nursery = nursery;
}

bool Kindergarten::getNursery() const
{
	return m_nursery;
}

void Kindergarten::setReporWidget(ReportKindergartenWidget * ptr)
{
	m_rep_kind_ptr = ptr;
}

ReportKindergartenWidget * Kindergarten::getReportWidget() const
{
	return m_rep_kind_ptr;
}

void Kindergarten::setWidget(ViewKindergartenWidget * ptr)
{
	m_kind_ptr = ptr;
}

ViewKindergartenWidget * Kindergarten::getWidget() const
{
	return m_kind_ptr;
}

std::string Kindergarten::typeString() const
{
	return std::string("Kindergarten");
}

char Kindergarten::img() const
{
	return 'k';
}

School::School()
{
}


School::~School()
{
}

void School::setAdress(std::string adress)
{
	m_adress = adress;
}

std::string School::getAdress() const
{
	return m_adress;
}

void School::setFloors(int floors)
{
	m_floors = floors;
}

int School::getFloors() const
{
	return m_floors;
}

void School::setName(std::string name)
{
	m_name = name;
}

std::string School::getName() const
{
	return m_name;
}

void School::setElectorateNumber(int number)
{
	m_electorate_number = number;
}

int School::getElectorateNumber() const
{
	return m_electorate_number;
}

void School::setReporWidget(ReportSchoolWidget * ptr)
{
	m_rep_school_ptr = ptr;
}

ReportSchoolWidget * School::getReportWidget() const
{
	return m_rep_school_ptr;
}

void School::setWidget(ViewSchoolWidget * ptr)
{
	m_school_ptr = ptr;
}

ViewSchoolWidget * School::getWidget() const
{
	return m_school_ptr;
}

std::string School::typeString() const
{
	return std::string("School");
}

char School::img() const
{
	return 'h';
}

University::University()
{
}


University::~University()
{
}

void University::setAdress(std::string adress)
{
	m_adress = adress;
}

std::string University::getAdress() const
{
	return m_adress;
}

void University::setFloors(int floors)
{
	m_floors = floors;
}

int University::getFloors() const
{
	return m_floors;
}

void University::setName(std::string name)
{
	m_name = name;
}

std::string University::getName() const
{
	return m_name;
}

void University::setRank(float rank)
{
	try {
		if (rank < 0.0 || rank > 5.0) {
			throw std::invalid_argument("Rank is not in the range {0.0, 0.5}");
		}
		m_rank = rank;
	}
	catch (const  std::invalid_argument &e) {
		std::cerr << e.what();
	}
}

float University::getRank() const
{
	return m_rank;
}

void University::setWidget(ViewUniversityWidget * ptr)
{
	m_uni_ptr = ptr;
}

ViewUniversityWidget * University::getWidget() const
{
	return m_uni_ptr;
}

std::string University::typeString() const
{
	return std::string("University");
}

char University::img() const
{
	return 'u';
}




ViewObjectWidget::ViewObjectWidget(Object * obj) : AbstractWidget(obj)
{
	m_obj_ptr = obj;
}

ViewObjectWidget::~ViewObjectWidget()
{

}

void ViewObjectWidget::show()
{
	std::cout
		<< m_obj_ptr->img() << std::endl
		<< m_obj_ptr->typeString() << std::endl;

}

ViewStopWidget::ViewStopWidget(Stop * stop)
	: ViewObjectWidget(stop)
{
	m_stop_ptr = stop;
}

ViewStopWidget::~ViewStopWidget()
{

}

void ViewStopWidget::show()
{
	std::cout
		<< m_stop_ptr->img() << std::endl
		<< m_stop_ptr->typeString() << std::endl
		<< m_stop_ptr->getAdress() << std::endl;

	for (auto i = m_stop_ptr->getRoutes().begin(); i != m_stop_ptr->getRoutes().end(); ++i) {
		std::cout << (*i) << std::endl;
	}
}

ViewSpecialConstructionWidget::ViewSpecialConstructionWidget(SpecialConstruction * sp_constr) :
	ViewSpecialConstructionWidget(sp_constr)
{
	m_sp_constr_ptr = sp_constr;
}

ViewSpecialConstructionWidget::~ViewSpecialConstructionWidget()
{
}

void ViewSpecialConstructionWidget::show()
{
	std::cout
		<< m_sp_constr_ptr->img() << std::endl
		<< m_sp_constr_ptr->typeString() << std::endl
		<< m_sp_constr_ptr->getAdress() << std::endl
		<< m_sp_constr_ptr->getFactory() << std::endl;
}

ViewBuildingWidget::ViewBuildingWidget(Building * build) : ViewObjectWidget(build)
{
	m_build_ptr = build;
}

ViewBuildingWidget::~ViewBuildingWidget()
{

}

void ViewBuildingWidget::show()
{
	std::cout
		<< m_build_ptr->img() << std::endl
		<< m_build_ptr->typeString() << std::endl
		<< m_build_ptr->getAdress() << std::endl
		<< m_build_ptr->getFloors() << std::endl;
}

ViewApartmentWidget::ViewApartmentWidget(Apartment * app) : ViewBuildingWidget(app)
{
	m_app_ptr = app;
}

ViewApartmentWidget::~ViewApartmentWidget()
{
}

void ViewApartmentWidget::show()
{
	std::cout
		<< m_app_ptr->img() << std::endl
		<< m_app_ptr->typeString() << std::endl
		<< m_app_ptr->getAdress() << std::endl
		<< m_app_ptr->getFloors() << std::endl
		<< m_app_ptr->getFlats() << std::endl;
}

ViewOfficeWidget::ViewOfficeWidget(Office * office) : ViewBuildingWidget(office)
{
	m_office_ptr = office;
}

ViewOfficeWidget::~ViewOfficeWidget()
{
}

void ViewOfficeWidget::show()
{
	std::cout
		<< m_office_ptr->img() << std::endl
		<< m_office_ptr->typeString() << std::endl
		<< m_office_ptr->getAdress() << std::endl
		<< m_office_ptr->getFloors() << std::endl;

	for (auto i = m_office_ptr->getCompanies().begin(); i != m_office_ptr->getCompanies().end(); ++i) {
		std::cout << (*i) << std::endl;
	}
}

ViewInstitutionWidget::ViewInstitutionWidget(Institution * inst) : ViewBuildingWidget(inst)
{
	m_inst_ptr = inst;
}

ViewInstitutionWidget::~ViewInstitutionWidget()
{
}

void ViewInstitutionWidget::show()
{
	std::cout
		<< m_inst_ptr->img() << std::endl
		<< m_inst_ptr->typeString() << std::endl
		<< m_inst_ptr->getAdress() << std::endl
		<< m_inst_ptr->getFloors() << std::endl
		<< m_inst_ptr->getName() << std::endl;
}

ViewKindergartenWidget::ViewKindergartenWidget(Kindergarten * kind) : ViewInstitutionWidget(kind)
{
	m_kind_ptr = kind;
}

ViewKindergartenWidget::~ViewKindergartenWidget()
{
}

void ViewKindergartenWidget::show()
{
	std::cout
		<< m_kind_ptr->img() << std::endl
		<< m_kind_ptr->typeString() << std::endl
		<< m_kind_ptr->getAdress() << std::endl
		<< m_kind_ptr->getFloors() << std::endl
		<< m_kind_ptr->getName() << std::endl
		<< m_kind_ptr->getNursery() << std::endl;
}

ViewSchoolWidget::ViewSchoolWidget(School * school) : ViewInstitutionWidget(school)
{
	m_school_ptr = school;
}

ViewSchoolWidget::~ViewSchoolWidget()
{
}

void ViewSchoolWidget::show()
{
	std::cout
		<< m_school_ptr->img() << std::endl
		<< m_school_ptr->typeString() << std::endl
		<< m_school_ptr->getAdress() << std::endl
		<< m_school_ptr->getFloors() << std::endl
		<< m_school_ptr->getName() << std::endl
		<< m_school_ptr->getElectorateNumber() << std::endl;
}

ViewUniversityWidget::ViewUniversityWidget(University * uni) : ViewInstitutionWidget(uni)
{
	m_uni_ptr = uni;
}

ViewUniversityWidget::~ViewUniversityWidget()
{

}

void ViewUniversityWidget::show()
{
	std::cout
		<< m_uni_ptr->img() << std::endl
		<< m_uni_ptr->typeString() << std::endl
		<< m_uni_ptr->getAdress() << std::endl
		<< m_uni_ptr->getFloors() << std::endl
		<< m_uni_ptr->getName() << std::endl
		<< m_uni_ptr->getRank() << std::endl;
}

ReportInstitutionWidget::ReportInstitutionWidget(Institution * inst) : AbstractWidget(inst)
{
	m_inst_ptr = inst;
}

ReportInstitutionWidget::~ReportInstitutionWidget()
{
}

void ReportInstitutionWidget::report()
{
}

void ReportInstitutionWidget::show()
{
}

ReportKindergartenWidget::ReportKindergartenWidget(Kindergarten * kind) : ReportInstitutionWidget(kind)
{
	m_kind_ptr = kind;
}

ReportKindergartenWidget::~ReportKindergartenWidget()
{
}

void ReportKindergartenWidget::report()
{
}

void ReportKindergartenWidget::show()
{
}

ReportSchoolWidget::ReportSchoolWidget(School * school) : ReportInstitutionWidget(school)
{
	m_school_ptr = school;
}

ReportSchoolWidget::~ReportSchoolWidget()
{
}

void ReportSchoolWidget::report()
{
}

void ReportSchoolWidget::show()
{
}

ReportUniversityWidget::ReportUniversityWidget(University * school) : ReportInstitutionWidget(school)
{
	m_school_ptr = school;
}

ReportUniversityWidget::~ReportUniversityWidget()
{
}

void ReportUniversityWidget::report()
{
}

void ReportUniversityWidget::show()
{
}

ListWidget::ListWidget() : AbstractWidget(nullptr)
{
	m_list = new std::list<Object *>();
}


ListWidget::~ListWidget()
{
}

void ListWidget::setList(std::list<Object*>* ptr)
{
	m_list = ptr;
}

std::list<Object*>* ListWidget::getList() const
{
	return m_list;
}

void ListWidget::append(Object * obj)
{
	m_list->push_back(obj);
}


void ListWidget::show()
{
	for (auto elem = m_list->begin(); elem != m_list->end(); ++elem)
	{
		std::cout
			<< (*elem)->img() << std::endl
			<< (*elem)->typeString() << std::endl;
	}
}
