#include "Object.h"
#include "Stop.h"
#include "SpecialConstruction.h"
#include "Apartment.h"
#include "Office.h"
#include "Kindergarten.h"
#include "School.h"
#include "University.h"

#include "ViewObjectWidget.h"
#include "ViewStopWidget.h"
#include "ViewSpecialConstructionWidget.h"
#include "ViewBuildingWidget.h"
#include "ViewApartmentWidget.h"
#include "ViewOfficeWidget.h"
#include "VeiwInstitutionWidget.h"
#include "ViewKindergartenWidget.h"
#include "ViewSchoolWidget.h"
#include "ViewUniversityWidget.h"

#include "ReportInstitutionWidget.h"
#include "ReportKindergartenWidget.h"
#include "ReportSchoolWidget.h"
#include "ReportUniversityWidget.h"

#include "ListWidget.h"

#include <list>
#include <stdexcept>
#include <iostream>

void showObject(Object *obj)
{
	ViewObjectWidget* viewWidget = obj->getWidget();
	if (viewWidget) {
		viewWidget->show();
	}
}

void showInstitutionReport(Institution *inst)
{
	ReportInstitutionWidget* reportWidget = inst->getReportWidget();
	if (reportWidget) {
		reportWidget->report();
	}
}

/*
void showAddWidget(Object *obj)
{
	AddObjectWidget* addWidget = obj->getAddWidget();
	if (addWidget) {
		viewWidget->add();
	}
}
*/

void fillObjectList(ListWidget *list)
{ 
	list = new ListWidget();

	std::list<Object *> objectList;

	for (auto elem = objectList.begin(); elem != objectList.end(); ++elem)
	{
		list->append(*elem);
	}
}


int main()
{
		
	std::system("pause");

    return 0;
}


