#pragma once

#include "VeiwInstitutionWidget.h"
#include "University.h"

class ViewUniversityWidget :
	public ViewInstitutionWidget
{
public:
	ViewUniversityWidget(University* uni);

	~ViewUniversityWidget();

	void show() override;

private:
	University * m_uni_ptr;
};

