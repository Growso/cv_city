#pragma once

#include "ViewObjectWidget.h"
#include "SpecialConstruction.h"

class ViewSpecialConstructionWidget :
	public ViewObjectWidget
{
public:
	ViewSpecialConstructionWidget(SpecialConstruction* sp_constr);

	~ViewSpecialConstructionWidget();

	void show() override;

private:
	SpecialConstruction * m_sp_constr_ptr;
};

