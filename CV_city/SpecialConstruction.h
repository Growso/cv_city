#pragma once

#include "AbstractCityItem.h"

class ViewSpecialConstructionWidget;

class SpecialConstruction :
	public AbstractCityItem
{

public:
	SpecialConstruction();

	~SpecialConstruction();
	
	void setAdress(std::string adress) override;
	std::string getAdress() const override;

	void setFactory(std::string factory);
	std::string getFactory() const;
	
	void setWidget(ViewSpecialConstructionWidget* ptr);
	ViewSpecialConstructionWidget* getWidget() const;

	std::string typeString() const override;

	char img() const override;

private:
	std::string						m_adress;
	std::string						m_factory;
	ViewSpecialConstructionWidget*	m_sp_costr_ptr;
};

