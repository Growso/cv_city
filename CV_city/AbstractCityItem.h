#pragma once

#include "Object.h"

class AbstractCityItem :
	public Object
{
public:
	virtual void setAdress(std::string adress) = 0;
	virtual std::string getAdress() const = 0;

	virtual std::string typeString() const = 0;

	virtual char img() const = 0;
};

