#pragma once

#include "Object.h"

class ListWidget :
	public AbstractWidget
{
public:
	ListWidget();

	~ListWidget();

	void setList(std::list<Object *> * ptr);
	std::list<Object *> * getList() const;

	void append(Object* obj);

	void show() override;

private:
	std::list<Object *> *    m_list;
};

