#pragma once

#include "Building.h"

class ViewApartmentWidget;

class Apartment :
	public Building
{
public:

	Apartment();

	~Apartment();

	void setFlats(int flats);
	int getFlats() const;

	void setAdress(std::string adress) override;
	std::string getAdress() const override;

	void setFloors(int floors) override;
	int getFloors() const override;

	void setWidget(ViewApartmentWidget* ptr);
	ViewApartmentWidget* getWidget() const;

	std::string typeString() const override;

	char img() const override;

private:	
	int						m_flats;
	ViewApartmentWidget*	m_appartment_ptr;
};

