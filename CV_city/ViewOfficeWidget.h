#pragma once

#include "ViewApartmentWidget.h"
#include "Office.h"

class ViewOfficeWidget :
	public ViewBuildingWidget
{
public:
	ViewOfficeWidget(Office* office);

	~ViewOfficeWidget();

	void show() override;

private:
	Office * m_office_ptr;
};

