#pragma once

#include "AbstractWidget.h"

class ViewObjectWidget :
	public AbstractWidget
{
public:

	ViewObjectWidget(Object *obj);

	virtual ~ViewObjectWidget();

	virtual void show() override;

private:
	Object * m_obj_ptr;
};

