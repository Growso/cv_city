#pragma once

#include <string>
#include <list>

class ViewObjectWidget;

class Object
{
public:
	Object();
	
	virtual ~Object();

	void setWidget(ViewObjectWidget* ptr);
	ViewObjectWidget* getWidget() const;

	virtual std::string typeString() const;

	virtual char img() const;

private:
	ViewObjectWidget* m_widget_ptr;
};

