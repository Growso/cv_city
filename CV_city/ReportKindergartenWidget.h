#pragma once

#include "ReportInstitutionWidget.h"
#include "Kindergarten.h"


class ReportKindergartenWidget :
	public ReportInstitutionWidget
{

public:

	ReportKindergartenWidget(Kindergarten *kind);

	~ReportKindergartenWidget();

	void report() override;

	void show() override;
	
private:
	Kindergarten * m_kind_ptr;
};

