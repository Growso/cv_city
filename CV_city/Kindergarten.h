#pragma once

#include "Institution.h"

class ViewKindergartenWidget;
class ReportKindergartenWidget;

class Kindergarten :
	public Institution
{
public:

	Kindergarten();

	~Kindergarten();

	void setAdress(std::string adress) override;
	std::string getAdress() const override;

	void setFloors(int floors) override;
	int getFloors() const override;

	void setName(std::string name) override;
	std::string getName() const override;
	
	void setNursery(bool nursery);
	bool getNursery() const;

	void setReporWidget(ReportKindergartenWidget* ptr);
	ReportKindergartenWidget* getReportWidget() const;

	void setWidget(ViewKindergartenWidget* ptr);
	ViewKindergartenWidget* getWidget() const;

	std::string typeString() const override;

	char img() const override;

private:
	bool						m_nursery;
	ViewKindergartenWidget*		m_kind_ptr;
	ReportKindergartenWidget*   m_rep_kind_ptr;
};

