#pragma once

#include "Institution.h"

class ViewSchoolWidget;
class ReportSchoolWidget;

class School :
	public Institution
{
public:

	School();

	~School();

	void setAdress(std::string adress) override;
	std::string getAdress() const override;

	void setFloors(int floors) override;
	int getFloors() const override;

	void setName(std::string name) override;
	std::string getName() const override;

	void setElectorateNumber(int number);
	int getElectorateNumber() const;

	void setReporWidget(ReportSchoolWidget* ptr);
	ReportSchoolWidget* getReportWidget() const;

	void setWidget(ViewSchoolWidget* ptr);
	ViewSchoolWidget* getWidget() const;

	std::string typeString() const override;

	char img() const override;

private:
	int						m_electorate_number;
	ViewSchoolWidget*		m_school_ptr;
	ReportSchoolWidget*		m_rep_school_ptr;
};

