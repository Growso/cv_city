#pragma once

#include "ViewBuildingWidget.h"
#include "Institution.h"

class ViewInstitutionWidget :
	public ViewBuildingWidget
{
public:
	ViewInstitutionWidget(Institution* inst);

	~ViewInstitutionWidget();

	void show() override;

private:
	Institution * m_inst_ptr;
};
