#pragma once

#include "AbstractCityItem.h"

class ViewStopWidget;

class Stop :
	public AbstractCityItem
{
public:

	Stop();

	~Stop();

	void setAdress(std::string adress) override;
	std::string getAdress() const override;

	void setRoutes(std::list<int> routes);
	std::list<int> getRoutes() const;

	void setWidget(ViewStopWidget* ptr);
	ViewStopWidget* getWidget() const;

	std::string typeString() const override;

	char img() const override;
		
private:
	std::string			m_adress;
	std::list<int>	    m_routes;
	ViewStopWidget*		m_widget_ptr;
};

