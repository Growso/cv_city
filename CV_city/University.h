#pragma once

#include "Institution.h"

class ViewUniversityWidget;

class University :
	public Institution
{
public:

	University();

	~University();

	void setAdress(std::string adress) override;
	std::string getAdress() const override;

	void setFloors(int floors) override;
	int getFloors() const override;

	void setName(std::string name) override;
	std::string getName() const override;
	
	void setRank(float rank);
	float getRank() const;

	void setWidget(ViewUniversityWidget* ptr);
	ViewUniversityWidget* getWidget() const;

	std::string typeString() const override;

	char img() const override;

private:
	float						m_rank;
	ViewUniversityWidget*		m_uni_ptr;
};

