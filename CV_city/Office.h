#pragma once

#include "Building.h"

class ViewOfficeWidget;
	
class Office :
	public Building
{
public:
	Office();

	~Office();

	void setCompanies(std::list<std::string>  companies);
	std::list<std::string> getCompanies() const;

	void setAdress(std::string adress) override;
	std::string getAdress() const override;

	void setFloors(int floors) override;
	int getFloors() const override;

	void setWidget(ViewOfficeWidget* ptr);
	ViewOfficeWidget* getWidget() const;

	std::string typeString() const override;

	char img() const override;

private:
	std::list<std::string>		m_companies;
	ViewOfficeWidget*			m_office_ptr;
};

