#pragma once

#include "ReportInstitutionWidget.h"
#include "School.h"

class ReportSchoolWidget :
	public ReportInstitutionWidget
{
public:

	ReportSchoolWidget(School *school);

	~ReportSchoolWidget();

	void report() override;

	void show() override;

private:
	School * m_school_ptr;
};
