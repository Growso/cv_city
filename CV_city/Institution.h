#pragma once

#include "Building.h"

class ReportInstitutionWidget;


class Institution :
	public Building
{
public:
	virtual ~Institution() {};

	virtual void setName(std::string name) = 0;
	virtual std::string getName() const = 0;

	void setReporWidget(ReportInstitutionWidget* ptr) {};
	ReportInstitutionWidget* getReportWidget() const 
	{
		return nullptr;
	};

protected:
	std::string			m_name;
};

