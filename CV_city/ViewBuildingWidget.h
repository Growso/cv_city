#pragma once

#include "ViewObjectWidget.h"
#include "Building.h"

class ViewBuildingWidget :
	public ViewObjectWidget
{
public:
	ViewBuildingWidget(Building* build);

	~ViewBuildingWidget();

	void show() override;

private:
	Building * m_build_ptr;
};

