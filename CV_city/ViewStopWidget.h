#pragma once

#include "ViewObjectWidget.h"
#include "Stop.h"

class ViewStopWidget :
	public ViewObjectWidget
{
public:
	ViewStopWidget(Stop* stop);

	~ViewStopWidget();

	void show() override;

private:
	Stop * m_stop_ptr;
};

